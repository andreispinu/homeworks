package login_signup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static final Scanner scan = new Scanner(System.in);
    public static HashMap<String,User> userData = new HashMap<>();
    public static char option;
    public static String username, password, day, month, year;
    public static boolean valid;

    public static void main(String[] args) {

        do{
            Messages.menuMessage();
            option=scan.next().charAt('\0');

            switch (option){
                case 'A':
                    valid = true;
                    Messages.menuMessageForFirstOption();

                    do {
                        System.out.print("Please insert your username: ");
                        username = scan.next();
                        System.out.print("Please select a password: ");
                        password = scan.next();

                        if(userData.get(username)!=null){
                            valid = false;
                        } else {
                            valid=true;
                        }

                    } while (!valid);


                    //We store new data in our username/password HashMap

                    System.out.print("Your day of birth: ");
                    day = scan.next();

                    int choice;
                    Messages.monthWereYouBornMessage();
                    choice = scan.nextInt();
                    switch (choice){
                        case 1: month = "JANUARY"; break;
                        case 2: month = "FEBRUARY"; break;
                        case 3: month = "MARCH"; break;
                        case 4: month = "APRIL"; break;
                        case 5: month = "MAY"; break;
                        case 6: month = "JUNE"; break;
                        case 7: month = "JULY"; break;
                        case 8: month = "AUGUST"; break;
                        case 9: month = "SEPTEMBER"; break;
                        case 10: month = "OCTOBER"; break;
                        case 11: month = "NOVEMBER"; break;
                        case 12: month = "DECEMBER"; break;

                    }

                    System.out.print("What is your year of birth? ");
                    year = scan.next();

                    //We store new data in our username/birthday HashMap

                    userData.put(username,new User(username, password, day+" "+month+" "+year));
                    break;

                case 'B':
                    System.out.print("Username: ");
                    username = scan.next();
                    System.out.print("Password: ");
                    password = scan.next();

                    if(userData.get(username) != null && userData.get(username).getPassword().equals(password)){
                        System.out.println("Welcome "+username+", your birthday is on "+userData.get(username).getBirthDay());
                    } else {
                        System.out.println("Incorect username or password. Please try again later.");
                        option='X';
                    }
                    break;
                default:
                    System.out.println("Try again.");
            }
        } while(option!='X');
    }

}
