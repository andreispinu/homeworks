package login_signup;

public class User {
    private String username;
    private String password;
    private String birthDay;

    User(String username, String password, String birthDay){
        setName(username);
        setPassword(password);
        setBirthDay(birthDay);
    }

    void setName(String username){
        this.username = username;
    }

    void setPassword(String password){
        this.password = password;
    }

    void setBirthDay(String birthDay){
        this.birthDay = birthDay;
    }

    String getName(){
        return username;
    }

    String getPassword(){
        return password;
    }

    public String getBirthDay(){
        return birthDay;
    }
}
