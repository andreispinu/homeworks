package login_signup;

public class Messages {
    static void menuMessage(){
        System.out.println("===================================================================");
        System.out.println("Welcome to our website. Select an option of what do you want to do:");
        System.out.println("A. Sign up");
        System.out.println("B. Login");
        System.out.println("X. Exit");
        System.out.println("===================================================================");
    }

    static void menuMessageForFirstOption(){
        System.out.println("===================================================================");
        System.out.println("||                       Create an account                       ||");
        System.out.println("===================================================================");
    }

    static void monthWereYouBornMessage(){
        System.out.println("===================================================================");
        System.out.println("What month were you born in? \n1: JANUARY \n2: FEBRUARY \n3: MARCH \n4: APRIL \n5: MAY \n6: JUNE \n7: JULY \n8: AUGUST \n9: SEPTEMBER \n10: OCTOBER \n11: NOVEMBER \n12: DECEMBER \nEnter your choice:");
        System.out.println("===================================================================");
    }
}
