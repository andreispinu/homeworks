public class Display {
    static void board(char[] array){
        System.out.println("/---|---|---\\");
        System.out.println("| "+array[1]+" | "+array[2]+" | "+array[3]+" |");
        System.out.println("|---|---|---|");
        System.out.println("| "+array[4]+" | "+array[5]+" | "+array[6]+" |");
        System.out.println("|---|---|---|");
        System.out.println("| "+array[7]+" | "+array[8]+" | "+array[9]+" |");
        System.out.println("\\---|---|---/");
    }
}
