
import java.util.Scanner;

class Main {

    static final Scanner scanner = new Scanner(System.in);
    static char[] elements = new char[10];
    static final int RADIX = 10;
    static int option;

    public static void main(String[] args) {
        boolean isOk = false;
        makeInitialBoard();

        for (int move = 1; move <= 9; move++) {

            if (move % 2 == 1) {
                takeOption("Chose your option X: ", 'X');
                if (move>=5 && Verification.checkIfWinner(option, elements)) break;
            } else {
                takeOption("Chose your option O:", 'O');
                if (move>=5 && Verification.checkIfWinner(option, elements)) break;
            }

            if (move == 9) {
                System.out.println("Equality, try again!");
            }
        }
    }


    private static void takeOption(String message, char x) {
        Display.board(elements);
        do {
            System.out.println(message);
            option = scanner.nextInt();
        } while (elements[option] == 'X' || elements[option] == 'O');
        elements[option] = x;
    }

    private static void makeInitialBoard() {
        for (int i = 1; i <= 9; i++) {
            elements[i] = Character.forDigit(i, RADIX);
        }
    }


}
