public class Verification {

	static boolean checkIfWinner(int option, char[] elements) {
		if (isTheWinner(option, elements[option], elements)) {
			System.out.println("Well done! " + elements[option] + " is the winner.");
			Display.board(elements);
			return true;
		}
		return false;
	}

	private static boolean isTheWinner ( int option, char _case, char[] elements){

		switch (option) {
			case 1:
				if (elements[2] == _case && elements[3] == _case) return true;
				if (elements[4] == _case && elements[7] == _case) return true;
				if (elements[5] == _case && elements[9] == _case) return true;
				break;
			case 2:
				if (elements[5] == _case && elements[8] == _case) return true;
				if (elements[1] == _case && elements[3] == _case) return true;
				break;
			case 3:
				if (elements[1] == _case && elements[2] == _case) return true;
				if (elements[5] == _case && elements[7] == _case) return true;
				if (elements[6] == _case && elements[9] == _case) return true;
				break;
			case 4:
				if (elements[5] == _case && elements[6] == _case) return true;
				if (elements[1] == _case && elements[7] == _case) return true;
				break;
			case 5:
				if (elements[1] == _case && elements[9] == _case) return true;
				if (elements[3] == _case && elements[7] == _case) return true;
				if (elements[4] == _case && elements[6] == _case) return true;
				if (elements[2] == _case && elements[8] == _case) return true;
				break;
			case 6:
				if (elements[4] == _case && elements[5] == _case) return true;
				if (elements[3] == _case && elements[9] == _case) return true;
				break;
			case 7:
				if (elements[3] == _case && elements[5] == _case) return true;
				if (elements[1] == _case && elements[4] == _case) return true;
				if (elements[8] == _case && elements[9] == _case) return true;
				break;
			case 8:
				if (elements[2] == _case && elements[5] == _case) return true;
				if (elements[7] == _case && elements[9] == _case) return true;
				break;
			case 9:
				if (elements[1] == _case && elements[5] == _case) return true;
				if (elements[3] == _case && elements[6] == _case) return true;
				if (elements[7] == _case && elements[8] == _case) return true;
				break;
		}

		return false;
	}
}
