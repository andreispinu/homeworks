package againaperson;

public class Employee extends Person {
	private Company company;
	private int salary;
	private int numberOfWorkedHoursPerWeek;

	public Employee(Company company, int salary, Person person) {
		super(person.getPersonName(), person.getPIN(), person.getAge(), person.getAdress());
		this.company = company;
		this.salary = salary;
	}

	//SETTERS:
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setNumberOfWorkedHoursPerWeek(int numberOfWorkedHoursPerWeek){
		this.numberOfWorkedHoursPerWeek = numberOfWorkedHoursPerWeek;
	}

	//GETTERS:
	public int getSalary() {
		return salary;
	}
	public Company getCompany() {
		return company;
	}
	public String getCompanyName() {
		return company.getCompanyName();
	}
	public String getCompanyCountryName() {
		return company.getCountryName();
	}
	public String getCompanyCountryCode() {
		return company.getCountryCode();
	}
	public boolean getCompanyIsEU() {
		return company.isEU();
	}
	@Override
	public int getNumberOfWorkedHoursPerWeek(){
		return numberOfWorkedHoursPerWeek;
	}

	@Override
	public String toString() {

		return "\n\nPerson: " + super.toString()
				+ "\n\nOcupation: Employee"
				+ "\nSalary: " + salary
				+ "\n\nCollege: " + company.toString();

	}

}
