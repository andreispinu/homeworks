package againaperson;

public class College{

	private Country country;
	private String schoolName;
	private String collegeName;

	public College(
			String collegeName,
			String schoolName,
			Country country
	){
		setCollegeName(collegeName);
		setSchoolName(schoolName);
		setCounty(country);
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public void setCounty(Country country) {
		this.country = country;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public Country getCountry() {
		return country;
	}

	public String getCountryName(){
		return country.getCountryName();
	}

	public String getCountryCode(){
		return country.getCountryCode();
	}

	public boolean isEU(){
		return country.isEU();
	}

	@Override
	public String toString(){
		return    "\nCollege name: " + collegeName
				+ "\nSchool name: " + schoolName
				+ country;
	}
}
