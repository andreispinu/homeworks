package againaperson;

public class Person{
	private String personName;
	private String PIN;
	private int age;
	private Adress adress;
	private int numberOfWorkedHoursPerWeek=0;

	public Person(){}

	public Person(String personName, String PIN, int age, Adress adress){
		setPersonName(personName);
		setPIN(PIN);
		setAge(age);
		setAdress(adress);
	}
	//SETTETS
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public void setPIN(String PIN) {
		this.PIN = PIN;
	}
	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	public void setAge(int age) {
		this.age = age;
	}

	// GETTERS
	public String getPersonName(){
		return this.personName;
	}
	public String getPIN() {
		return PIN;
	}
	public int getAge() {
		return age;
	}
	public Adress getAdress() {
		return adress;
	}
	public String getCityName(){
		return adress.getCityName();
	}
	public String getPostalCode(){
		return adress.getPostalCode();
	}
	public String getCountryName(){
		return adress.getCountyName();
	}
	public String getCountryCode(){
		return adress.getCountryCode();
	}
	public boolean isEU(){
		return adress.isEU();
	}
	public int getNumberOfWorkedHoursPerWeek(){
		return numberOfWorkedHoursPerWeek;
	}

	@Override
	public String toString(){
		return "\nName: " + personName
				+ "\nPersonal identification number: " + PIN
				+ "\nAge: " + age
				+ adress;
	}
}
