package againaperson;

public class Country {
	private String countryName;
	private String countryCode;
	private boolean isEU;

	public Country() {}

	public Country(
			String countryName,
			String countryCode,
			boolean isEU
	){
		this.countryName = countryName;
		this.countryCode = countryCode;
		this.isEU = isEU;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public boolean isEU() {
		return isEU;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setEU(boolean isEU) {
		this.isEU = isEU;
	}

	@Override
	public String toString(){
		return    "\nCountry name: " + countryName
				+ "\nCountry code: " + countryCode
				+ "\nIs in European Union: " + isEU;
	}
}
