package againaperson;

import javax.swing.plaf.PanelUI;

public class Unemployed extends Person{
	private int freeTime;
	private String basicJob;
	private int numberOfWorkedHoursPerWeek=0;

	public Unemployed(){}

	public Unemployed(String basicJob, int freeTime, Person person){
		super(person.getPersonName(),person.getPIN(),person.getAge(),person.getAdress());
		this.freeTime = freeTime;
		this.basicJob = basicJob;
	}

	//SETTERS
	public void setFreeTime(int freeTime) {
		this.freeTime = freeTime;
	}
	public void setBasicJob(String basicJob) {
		this.basicJob = basicJob;
	}

	//GETTERS
	public String getBasicJob() {
		return basicJob;
	}
	public int getFreeTime() {
		return freeTime;
	}

	@Override
	public String toString(){
		return "\n\nPerson: " + super.toString()
				+ "\n\nOcupation: Unemployed"
				+ "\nBasic job: " + basicJob
				+ "\nFree time: " + freeTime;
	}
}
