package againaperson;

public class Company{
	private String companyName;
	private Country country;

	public Company(){}

	public Company(
			String companyName,
			Country country
	){
		setCompanyName(companyName);
		setCountry(country);
	}

	//SETTERS:

	public void setCountry(Country country) {
		this.country = country;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	//GETTERS:

	public String getCompanyName(){
		return companyName;
	}
	public Country getCountry() {
		return country;
	}
	public String getCountryName(){
		return country.getCountryName();
	}
	public String getCountryCode(){
		return country.getCountryCode();
	}
	public boolean isEU(){
		return country.isEU();
	}

	@Override
	public String toString(){
		return "\nCompany name: " + companyName
				+ country;
	}
}
