package againaperson;

public class Adress{
	private String cityName;
	private String postalCode;
	private Country country;

	public Adress(){}

	public Adress(
			String cityName,
			String postalCode,
			Country country
	){
		setCityName(cityName);
		setPostalCode(postalCode);
		setCountry(country);
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityName() {
		return cityName;
	}

	public String getCountyName() {
		return country.getCountryName();
	}

	public String getCountryCode(){
		return country.getCountryCode();
	}

	public Country getCountry(){
		return country;
	}

	public boolean isEU(){
		return country.isEU();
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setCountry(Country country){
		this.country = country;
	}

	@Override
	public String toString(){
		return    "\nCity: " + cityName
				+ "\nPostal code: " + postalCode
				+ country;
	}
}

