package againaperson;

public class Test {
	public static void main(String[] args) {
		Country romania = new Country("Romania", "70",true);
		Country germany = new Country("Germany", "52", true);

		Adress iasi = new Adress("Iasi","700444",romania);
		Adress munich = new Adress("Munich", "520300",germany);

		Company realEstate = new Company("Real Estate", germany);
		Company ITCompany = new Company("ITCompany", romania);

		College computerScience = new College("University Alexandru Ioan Cuza", "Computer Science", romania);
		College mathematics = new College("Technical University of Munich", "Mathematics", germany);

		Person george = new Person("George", "20050050030", 20, munich);
		Person andrei = new Person("Andrei", "1800202222222",30,iasi);
		Person john = new Person("Jhon", "20125820515", 45, munich);

		Student georgeStudent = new Student(2, computerScience, george);

		Employee andreiEmployee = new Employee(ITCompany, 10000, andrei);
		andreiEmployee.setNumberOfWorkedHoursPerWeek(60);

		Unemployed johnUnemplyed = new Unemployed("Real Estate Agent", 12, john);

		System.out.println(georgeStudent);

		System.out.println(andreiEmployee);
		System.out.println(andreiEmployee.getNumberOfWorkedHoursPerWeek());

		System.out.println(johnUnemplyed);
	}
}
