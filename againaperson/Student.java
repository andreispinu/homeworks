package againaperson;

public class Student extends Person{
	private College college;
	private int studyYear;
	private int numberOfWorkedHoursPerWeek=0;

	public Student(){}

	public Student(int studyYear, College college, Person person){
		super(person.getPersonName(),person.getPIN(),person.getAge(),person.getAdress());
		this.college = college;
		this.studyYear = studyYear;
	}

	//setters
	public void setCollege(College college) {
		this.college = college;
	}
	public void setStudyYear(int studyYear) {
		this.studyYear = studyYear;
	}

	//getters
	public College getCollege() {
		return college;
	}
	public int getStudyYear() {
		return studyYear;
	}
	public String getCollegeName(){
		return college.getCollegeName();
	}
	public String getCollegeCountryName(){
		return college.getCountryName();
	}
	public String getCollegeCountryCode(){
		return college.getCountryCode();
	}
	public boolean getCollegeCountryIsEU(){
		return college.isEU();
	}

	@Override
	public String toString(){
		return    "\n\nPerson: " + super.toString()
				+ "\n\nOcupation: Student"
				+ "\nStudy year: " + studyYear
				+ "\n\nCollege: " + college.toString();
	}
}
